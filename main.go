package main

import (
	"flag"
	"io/ioutil"
	"net/http"
	"strconv"
	"log"
	"time"
	"strings"

	"periph.io/x/periph/host"
	"periph.io/x/periph/conn/gpio"
	"periph.io/x/periph/conn/gpio/gpioreg"
)

var (
	listenAddress = flag.String("web.listen-address", ":9172", "The address to listen on for HTTP requests.")
	metricsPath   = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics.")
	pin           = flag.String("config.pin", "GPIO18", "GPIO Pin to monitor")
	metric        = flag.String("config.metric", "power_meter_kwh_total", "Metric name")
	storage       = flag.String("config.storage", "ticks.txt", "File where the current tick count is stored")
	tick          = flag.Uint64("config.tick", 1, "Number of watt hours per tick")
)

var ticks uint64 = 0

func loop() {
	ticksFile, err := ioutil.ReadFile(*storage)
	if err == nil {
		ticks, _ = strconv.ParseUint(strings.TrimSpace(string(ticksFile)), 10, 64)
	}
	if _, err := host.Init(); err != nil {
		log.Fatal(err)
	}
	p := gpioreg.ByName(*pin)
	if p == nil {
		log.Fatal("Failed to find " + *pin)
	}
	p.In(gpio.PullDown, gpio.RisingEdge)
	for {
		if p.WaitForEdge(-1) {
			time.Sleep(25 * time.Millisecond)
			if p.Read() != gpio.High {
				continue // Signal is at least 30ms long, so after 25ms it should still be high.
			}
			ticks++
			ioutil.WriteFile(*storage, []byte(strconv.FormatUint(ticks, 10)), 0644)
			time.Sleep(100 * time.Millisecond) // Signal is up to 120ms long, so after 125ms it should be down again.
		}
	}
}

func main() {
	flag.Parse()
	go loop()
	http.HandleFunc("/metrics", func(res http.ResponseWriter, req *http.Request) {
		res.Write([]byte(*metric + " " + strconv.FormatFloat(float64(ticks * *tick) / 1000, 'f', 3, 64) + "\n"))
	})
	log.Printf("Listening on %s\n", *listenAddress)
	if err := http.ListenAndServe(*listenAddress, nil); err != nil {
		log.Fatalf("Error starting HTTP server: %s", err)
	}
}
