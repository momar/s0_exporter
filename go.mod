module codeberg.org/momar/s0_exporter

require (
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/prometheus/client_golang v1.6.0
	github.com/prometheus/common v0.10.0
	gopkg.in/yaml.v2 v2.3.0
	periph.io/x/periph v3.6.3+incompatible
)
